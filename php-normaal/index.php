<?php
require_once "db.php";
require_once "header.php";

//query don't delete or it will stop working.
    $sql = "SELECT * FROM taken";

    if ($result = mysqli_query($connection, $sql)) {
        include "addtaak.php";
        ?>

        <br>
        <div id="taken-container" xmlns="http://www.w3.org/1999/html">
            <?php
            while ($row = mysqli_fetch_assoc($result)) {
                ?>

                <div id="taak-container">
                    <p id="taak-naam"><?php echo $row['taaknaam']; ?></p>
                    <p id="taak-datum"><?php echo $row['datum']; ?></p>
                    <p id="taak-beschrijving"><?php echo $row['taakbeschrijving']; ?></p>
                    <p><a href="updatetaak.php?id=<?php echo $row['taakid'];?>">Update taak</a></p>
                    <p><a href="deletetaak.php?id=<?php echo $row['taakid'];?>">delete taak</a></p>
                </div>

                <?php
            }
            ?>
        </div>
        <?php
    } else {
        ?>
        <a href="addtaak.php">voeg taak toe</a>
        <?php
        echo 'Geen taken beschikbaar';
    }