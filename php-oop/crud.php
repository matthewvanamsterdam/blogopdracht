<?php
require_once 'db.php';

Class Crud extends Db
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create()
    {
        $userid = 1;
        $taaknaam = mysqli_real_escape_string($this->connection, $_POST['taaknaam']);
        $taakbeschrijving = mysqli_real_escape_string($connection, $_POST['taakbeschrijving']);
        $datum = $_POST['datum'];

        $sql = "INSERT INTO taken(userid, taaknaam, taakbeschrijving, datum) VALUES ('$userid','$taaknaam' ,'$taakbeschrijving', '$datum')";

        if (mysqli_query($connection, $sql)) {
            echo "Records added successfully.";
            header("refresh:0; url=index.php");
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($connection);
            header("refresh:0; url=index.php");
        }

        mysqli_close($connection);
    }

    public function read()
    {
        $sql = "SELECT * FROM taken";

        $result = mysqli_query($this->connection, $sql);

        ?>

        <div id="taken-container" xmlns="http://www.w3.org/1999/html">
            <?php
            while ($row = mysqli_fetch_assoc($result)) {
                ?>
                <div id="taak-container">
                    <p id="taak-naam"><?php echo $row['taaknaam']; ?></p>
                    <p id="taak-datum"><?php echo $row['datum']; ?></p>
                    <p id="taak-beschrijving"><?php echo $row['taakbeschrijving']; ?></p>
                    <p><a href="updatetaak.php?id=<?php echo $row['taakid']; ?>">Update taak</a></p>
                    <p><a href="deletetaak.php?id=<?php echo $row['taakid']; ?>">delete taak</a></p>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }

}