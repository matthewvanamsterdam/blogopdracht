<?php
session_start();
require_once "db.php";
if(isset($_SESSION['login_status']))
{
    echo 'Je bent niet ingelogd';
    header("refresh:3; url=index.php");
}
else {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $sgebruiker = trim($_POST['username']);
        $spassword = trim($_POST['password']);
        $spassword = hash('sha256', $spassword);

        $query = "Select username,password,userid 
                      From users
                      Where 
                      username ='$sgebruiker' 
                      AND password ='$spassword'
                      ";

        if ($resultaat = mysqli_query($connection, $query)) {
            $aantal = mysqli_num_rows($resultaat);

            if ($aantal == 1) {
                $_SESSION["login_status"] = TRUE;
                $_SESSION["gebruiker"] = $sgebruiker;

                $row = mysqli_fetch_assoc($resultaat);
                $_SESSION["userid"] = $row['userid'];
                mysqli_close($connection);

                echo "You are logged in now please you will be redirected shortly after.";
                header('refresh:2; url=index.php');
            } else {
                echo "One of the fields is not correct, please try again.";
                header('refresh:2; url=index.php');
            }
        }
    } else {
        echo "
    <form action='login.php' method='post'>
        <input id='username' name='username' type='text'>
        <input id='password' name='password' type='password'>
        <input type='submit'>
    </form>
    ";
    }
}