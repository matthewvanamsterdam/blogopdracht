<?php
session_start();
require_once "db.php";
require_once "header.php";

if (isset($_SESSION['login_status'])) {
    echo 'Userid: ' . $_SESSION['userid'] . '<br>';
    echo 'Gebruikersnaam: ' . $_SESSION['gebruiker'] . '<br>';

    $sql = "SELECT * FROM taken";


    if ($result = mysqli_query($connection, $sql)) {
        ?>
        <div id="taken-container">
            <?php
            while ($row = mysqli_fetch_assoc($result)) {
                ?>
                <div id="taak-container">
                    <p id="taak-naam"><?php echo $row['taaknaam']; ?></p>
                    <p id="taak-datum"><?php echo $row['datum']; ?></p>
                    <p id="taak-beschrijving"><?php echo $row['taakbeschrijving']; ?></p>
                    <p><a href="updatetaak.php?id=<?php echo $row['taakid'];?>">Update taak</a></p>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    } else {
        echo 'Geen taken beschikbaar';
    }
} else {
    echo "<a href='login.php'><button>Log hier in</button></a>";
}
