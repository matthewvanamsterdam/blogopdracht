<?php
session_start();
if(!isset($_SESSION['login_status']))
{
    echo 'Je bent niet ingelogd';
    header("refresh:3; url=login.php");
}
else {

    if (isset($_POST['taaknaam']) == true && isset($_POST['taakbeschrijving']) == true) {

        require_once "db.php";

        $userid = $_SESSION['userid'];
        $taaknaam = mysqli_real_escape_string($connection, $_POST['taaknaam']);
        $taakbeschrijving = mysqli_real_escape_string($connection, $_POST['taakbeschrijving']);
        $datum = $_POST['datum'];

        $sql = "INSERT INTO taken(userid, taaknaam, taakbeschrijving, datum) VALUES ('$userid','$taaknaam' ,'$taakbeschrijving', '$datum')";

        if (mysqli_query($connection, $sql)) {
            echo "Records added successfully.";
            header("refresh:2; url=addtaak.php");
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($connection);
            header("refresh:4; url=addtaak.php");
        }

        mysqli_close($connection);
    }
        ?>

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Add Record Form</title>
        </head>
        <body>
        <form action="addtaak.php" method="post">
            <p>
                <label for="taaknaam">Taaknaam</label>
                <input type="text" name="taaknaam">
            </p>
            <p>
                <label for="taakbeschrijving">Beschrijving</label>
                <input type="text" name="taakbeschrijving">
            </p>
            <p>
                <label for="datum">Datum</label>
                <input type="date" name="datum">
            </p>
            <input type="submit" value="Submit">
        </form>
        </body>
        </html>

        <?php
}


